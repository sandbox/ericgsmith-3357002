<?php

namespace Drupal\Tests\facets_missing_merge\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\facets\Entity\Facet;
use Drupal\facets\Processor\ProcessorPluginManager;
use Drupal\facets\Result\Result;
use Drupal\facets_missing_merge\Plugin\facets\processor\MissingItemMergeProcessor;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Unit test for processor.
 *
 * @group facets_missing_merge
 */
class MissingItemMergeProcessorTest extends UnitTestCase {

  /**
   * The processor to be tested.
   *
   * @var \Drupal\facets\Processor\BuildProcessorInterface
   */
  protected $processor;

  /**
   * An array containing the results before the processor has ran.
   *
   * @var \Drupal\facets\Result\Result[]
   */
  protected $originalResults;

  /**
   * Creates a new processor object for use in the tests.
   */
  protected function setUp(): void {
    parent::setUp();

    $facet = new Facet([], 'facets_facet');
    $facet->setMissing(TRUE);
    $missingResult = new Result($facet, '!', 'none', 20);
    $missingResult->setMissing(TRUE);
    $missingResult->setMissingFilters(['llama', 'badger', 'duck']);
    $this->originalResults = [
      new Result($facet, 'llama', 'Big Llama', 10),
      new Result($facet, 'badger', 'Tiny Badger', 5),
      new Result($facet, 'duck', 'Large Duck', 15),
      $missingResult,
    ];

    $processor_id = 'missing_item_merge';
    $this->processor = new MissingItemMergeProcessor([], $processor_id, []);

    $processor_definitions = [
      $processor_id => [
        'id' => $processor_id,
        'class' => MissingItemMergeProcessor::class,
      ],
    ];

    $manager = $this->createMock(ProcessorPluginManager::class);
    $manager->expects($this->any())
      ->method('getDefinitions')
      ->willReturn($processor_definitions);
    $manager->expects($this->any())
      ->method('createInstance')
      ->willReturn($this->processor);

    $container_builder = new ContainerBuilder();
    $container_builder->set('plugin.manager.facets.processor', $manager);
    $container_builder->set('string_translation', $this->getStringTranslationStub());

    \Drupal::setContainer($container_builder);
  }

  /**
   * Tests filtering happens for raw value.
   */
  public function testRawValueFilter() {
    $this->testItemMergedConfiguration(
      ['target_item' => 'llama'],
      ['badger', 'duck'],
      'Big Llama (or none)',
      30
    );
  }

  /**
   * Tests filtering happens for display value.
   */
  public function testDisplayValueFilter() {
    $this->testItemMergedConfiguration(
      ['target_item' => 'Tiny Badger'],
      ['llama', 'duck'],
      'Tiny Badger (or none)',
      25
    );
  }

  /**
   * Tests filtering happens for specific config.
   *
   * @param array $config
   *   The plugin configuration.
   * @param array $expected_filters
   *   The expected filters of the merged item.
   * @param string $expected_label
   *   The expected label of the merged item.
   * @param int $expected_count
   *   The expected count of the merged item.
   */
  protected function testItemMergedConfiguration(array $config, array $expected_filters, string $expected_label, int $expected_count): void {
    $facet = new Facet([], 'facets_facet');
    $facet->setMissing(TRUE);
    $facet->setResults($this->originalResults);
    $facet->addProcessor([
      'processor_id' => 'exclude_specified_items',
      'weights' => [],
      'settings' => $config,
    ]);
    $this->processor->setConfiguration($config);

    $filtered_results = $this->processor->build($facet, $this->originalResults);

    // Count is one less than the original as the item has been merged.
    $this->assertCount((count($this->originalResults) - 1), $filtered_results);

    $found_missing = FALSE;
    foreach ($filtered_results as $result) {
      if ($result->isMissing()) {
        $found_missing = TRUE;
        // Merged value is no longer in the filters.
        $this->assertSame($expected_filters, $result->getMissingFilters());
        // Label has been updated.
        $this->assertSame($expected_label, $result->getDisplayValue());
        // Count is updated.
        $this->assertSame($expected_count, $result->getCount());
      }
    }
    $this->assertTrue($found_missing);
  }

}
